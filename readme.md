# Instalación
- composer install
- php artisan vendor:publish --tag=lang

## Notas:
Tener en cuenta que el datapicker que está en memberships/new. Se agrega sólo la carpeta de bootstrap-datepicker del vendor.

## Referencias:
Usar y refrescar idiomas: https://github.com/Laraveles/spanish
Ejemplos relaciones laravel - one to many: https://www.larashout.com/introducing-one-to-many-relationship-in-laravel
Ejemplos relaciones laravel - one to one: https://www.larashout.com/introducing-one-to-one-relationship-in-laravel
Ejemplo manejo de requests: https://www.youtube.com/watch?v=-QapNzUE4V0
Ejemplo many to many: https://www.larashout.com/laravel-eloquent-many-to-many-relationship
CRUD y mensajes: https://www.youtube.com/watch?v=-QapNzUE4V0
Utilizar VueJS: https://laracasts.com/discuss/channels/vue/why-is-my-vue-in-laravel-not-showing?page=1
Documentación Spatie: https://docs.spatie.be/laravel-permission/v3/basic-usage/blade-directives/
Base de datos de minucipios: https://github.com/vafelu/departamentos-y-municipios-colombia-SQL
Plugin Workbench y laravel migrations: https://laravel-news.com/export-from-mysql-workbench-to-a-laravel-migration
Importante: cuando se migre las tablas de la configuracion del Workbench, hay que agregar el $table->timestamps(); en cada tabla.
Relaciones polimorficas: https://stackoverflow.com/questions/40817211/laravel-morph-relationship
Referencia login: https://medium.com/@cvallejo/sistema-de-autenticaci%C3%B3n-api-rest-con-laravel-5-6-240be1f3fc7d
Reference soft delete: https://www.morgandavison.com/2015/05/04/adding-soft-deletes-to-existing-database-table-in-laravel/
Find inside a relation: https://stackoverflow.com/questions/51895356/laravel-5-4-how-to-find-specific-record-in-relation
Actualizar timestamp en tablas pivot delacionadas: https://stackoverflow.com/questions/25726602/timestamps-are-not-updating-while-attaching-data-in-pivot-table
Agregar owner y usuarios asignados: https://laracasts.com/discuss/channels/eloquent/many-to-many-with-single-owner
Modelos dinamicos: https://laracasts.com/discuss/channels/laravel/dynamic-model-names?page=1

## Librerias:
Drag and drop: https://github.com/SortableJS/Vue.Draggable

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
