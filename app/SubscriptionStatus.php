<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionStatus extends Model
{
    //
    public function subscription(){
        return $this->hasOne(SubscriptionPack::class);
    }
}
