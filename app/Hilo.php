<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Hilo extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'description', 
        'date_ini', 
        'date_end', 
        'status_id', 
        'priority_id', 
        'subscription_pack_id',
        'created_at',
        'updated_at',
    ];

    public function subscription(){
        return $this->belongsTo(SubscriptionPack::class, 'subscription_pack_id');
    }
    
    public function tasks(){
        return $this->hasMany(HiloCheckList::class);
    }

    public function moderators(){
        return $this->belongsToMany(User::class)->withTimestamps();
        // return $this->hasMany(User::class);
    }
    public function status(){
        return $this->belongsTo(Status::class);
    }
    public function priority(){
        return $this->belongsTo(Priority::class);
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function documents() {
        return $this->morphMany(Document::class, 'documentable');
    }
}
