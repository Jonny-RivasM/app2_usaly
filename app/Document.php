<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
      'documentable_id',
      'documentable_type',
      'path'
    ];

    public function documentable(){
      return $this->morphTo();
    }
}
