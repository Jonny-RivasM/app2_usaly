<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    
    protected $fillable = [
        'name',
        'num_id',
        'address',
        'phone',
        'municipio_id'
    ];

    // public function subscription(){
    //     return $this->belongsTo(SubscriptionPack::class);
    // }
    public function subscription(){
        // return $this->hasOne(SubscriptionPack::class);
        return $this->hasMany(SubscriptionPack::class)->withTimestamps();
    }

    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function documents(){
      return $this->morphMany(Document::class, 'documentable');
    }  
}
