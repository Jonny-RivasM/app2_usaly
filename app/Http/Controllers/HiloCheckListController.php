<?php

namespace App\Http\Controllers;

use App\HiloCheckList;
use App\Package;
use App\Hilo;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Traits\HiloTraits;
use Carbon\Carbon;

class HiloCheckListController extends Controller
{
    use HiloTraits;

    /**
     * List all users, it's optional return users deleted
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $numPerPages = Config::get( 'constants.records_per_page' );
        $tasks = HiloCheckList::with('hilo')->paginate( $numPerPages );
        return response()->json($tasks, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $tasks = HiloCheckList::find( $id );
        return response()->json($tasks, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateTaskHilo( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 
            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);
        }

        $hilo = Hilo::find( $request->input('hilo_id') );
        if( !$hilo ){
            return response()->json( [
                'status' => 'error',
                'message' => __('hilo.not_exists')
            ], 404);   
        }

        $task = HiloCheckList::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'hilo_id' => $request->input('hilo_id'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $userLogged = User::find( auth()->user()->id )->with('roles')->first();

        if ( $request->input('completed') ) {
            // validate permissions
            if( !$userLogged->hasRole('admin') || !$userLogged->hasRole('moderator') ){
                return response()->json( [
                    'status' => 'error',
                    'message' => __('general.no_permissions'),
                    'errors' => $validator->errors(),
                ], 400);
            }
            $task->completed = $request->input('completed');
        }

        $task->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('hilo.task_create'),
            'task' => $task
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateTaskHilo( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 
            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }
            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }

        $hilo = Hilo::where( 'id', $id )->first();
        
        if( !$hilo ){
            return response()->json( [
                'status' => 'error',
                'message' => __('hilo.not_exists')
            ], 404);   
        }

        $task = HiloCheckList::where( 'id', $id )->first();
        
        if( !$task ){
            return response()->json( [
                'status' => 'error',
                'message' => __('hilo.task_not_exists')
            ], 404);   
        }

        $userLogged = User::find( auth()->user()->id )->with('roles')->first();
        if ( $request->input('completed') ) {
            // validate permissions
            if( !$userLogged->hasRole('admin') || !$userLogged->hasRole('moderator') ){
                return response()->json( [
                    'status' => 'error',
                    'message' => __('general.no_permissions'),
                    'errors' => $validator->errors(),
                ], 400);
            }
            if( $request->input('completed') ){
                $task->completed = $request->input('completed');
            }
            if( $request->input('hilo_id') ){
                $task->hilo_id = $request->input('hilo_id');
            }
        }


        if( $request->input('name') ){
            $task->name = $request->input('name');
        }
        if( $request->input('description') ){
            $task->description = $request->input('description');
        }

        $task->save();

        $msj = __('general.data_no_change');
        if( $task->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'task' => $task
        ), 200); 
    }

    /**
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $task = HiloCheckList::find( $id );
        $task->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('hilo.task_delete')
        ), 200);
    }
}
