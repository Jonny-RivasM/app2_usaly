<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;

class StatusController extends Controller
{
    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $status = Status::all();
        return response()->json($status, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $status = Status::find( $id );
        return response()->json($status, 200); 
    }
}
