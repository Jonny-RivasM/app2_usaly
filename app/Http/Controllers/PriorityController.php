<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Priority;

class PriorityController extends Controller
{
    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $priority = Priority::all();
        return response()->json($priority, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $priority = Priority::find( $id );
        return response()->json($priority, 200); 
    }
}
