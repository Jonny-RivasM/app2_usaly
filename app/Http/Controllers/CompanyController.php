<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Traits\CompanyTraits;

class CompanyController extends Controller
{
    use CompanyTraits;

    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $numPerPages = Config::get( 'constants.records_per_page' );
        $companies = Company::paginate( $numPerPages );
        return response()->json($companies, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $company = Company::find( $id );
        return response()->json($company, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateCompany( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('company.company_validator'),
                'errors' => $errors,
            ], 400);
        }

        $company = Company::create([
            'name' => $request->input('name'),
            'num_id' => $request->input('num_id'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'municipio_id' => $request->input('municipio_id'),
        ]);

        
        if( $request->file('logo') ) {
            $company->logo = $this->uploadLogoCompany( $request->file('logo'), $company->id );
            $company->save();
        }

        return response()->json( array(
            'status' => 'success',
            'message' => __('company.company_create'),
            'company' => $company
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateCompany( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('company.company_validator'),
                'errors' => $errors,
            ], 400);
        }

        $userLogged = User::find( auth()->user()->id )->with('roles')->first();
        $company = Company::where( 'id', $id )->first();
        
        if( !$company ){
            return response()->json( [
                'status' => 'error',
                'message' => __('company.not_exists')
            ], 404);   
        }

        // validate permissions
        if( !$userLogged->hasRole('admin') ){
            if ( array_key_exists( 'num_id',  $request->all() ) ) { 
                return response()->json( [
                    'status' => 'error',
                    'message' => __('general.no_permissions'),
                    'errors' => $validator->errors(),
                ], 400);   
            }
        }

        if( $request->file('logo') ) {
            $company->logo = $this->changeLogoCompany( $request->file('logo'), $company->logo, $company->id );
            $company->save();
        }

        if( $request->input('name') ){
            $company->name = $request->input('name');
        }
        if( $request->input('num_id') ){
            $company->num_id = $request->input('num_id');
        }
        if( $request->input('address') ){
            $company->address = $request->input('address');
        }
        if( $request->input('phone') ){
            $company->phone = $request->input('phone');
        }
        if( $request->input('municipio_id') ){
            $company->municipio_id = $request->input('municipio_id');
        }

        $company->save();

        $msj = __('general.data_no_change');
        if( $company->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'company' => $company
        ), 200); 
    }

    public function addUser(Request $request, $idCompany, $idUser) {

        $user = User::find($idUser);
        
        if( !$user ){
            return response()->json(array(
                'status' => 'success',
                'message' => __('user.not_exists')
            ), 200);
        }
        
        $company = Company::with('users')->where('id', $idCompany)->first();

        if( !$company ){
            return response()->json(array(
                'status' => 'success',
                'message' => __('user.not_exists')
            ), 200);
        }
        
        $userExistsInCompay = $company->users()->whereKey($user)->exists();

        // Verify if user exists inside a company
        if( $userExistsInCompay ) {
            return response()->json(array(
                'status' => 'success',
                'message' => __('user.already_exists_company')
            ), 200);
        }

        $company->users()->attach( $user );
        $company->save();

        return response()->json(array(
            'status' => 'success',
            'message' => __('company.user_associated')
        ), 200); 
    }

    public function old_ajaxChangeLogo(Request $request, $idCompany) {

        if( !$idCompany ){
            return response()->json([
                'status' => 'error', 
                'message'=>__('ID Company does not be empty')
            ]);
        }

        $file = $request->file('logo');
        if( !$file ){
            return response()->json([
                'status' => 'error', 
                'message'=>__('The logo does not be empty')
            ]);
        }
        $company = Company::find( $idCompany );

        // return response()->json(is_file( public_path().'/'.$company->logo ));
        if(is_file( public_path().'/'.$company->logo )){
            // Remove current image
            unlink(public_path().'/'.$company->logo);
        }


        $nameFile = time().$file->getClientOriginalName();
        // Add new image
        $file->move('companies/logos/', $nameFile );
        
        $company->logo = 'companies/logos/'.$nameFile;
        $company->save();

        return response()->json([
            'status' => 'success', 
            'message'=>__('Company logo changed successfully'),
            'company' => $company
        ]);
    }
    
    public function old_ajaxUpdateCompany(Request $request, $idCompany){

        if( !$idCompany ){
            return response()->json([
                'status' => 'error', 
                'message'=>__('ID Company does not be empty')
            ]);
        }

        $inputField = $request->input('key');

        $company = Company::find( $idCompany );
        $company->{$inputField} = $request->input('value');
        $company->save();

        return response()->json([
            'status' => 'success', 
            'message'=>__('Data of company is updated successfully')
        ]);
    }
}
