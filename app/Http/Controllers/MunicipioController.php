<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use App\Traits\LocationTraits;
use Illuminate\Http\Request;
use App\Municipio;

class MunicipioController extends Controller
{
    use LocationTraits;

    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $soft = $request->input('soft');
        $numPerPages = Config::get( 'constants.records_per_page' );
        if( $soft ){
            $municipios = Municipio::onlyTrashed()->paginate(10);
        }else{
            $municipios = Municipio::paginate(10);
        }
        return response()->json($municipios, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $municipio = Municipio::find( $id );
        return response()->json($municipio, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateMunicipio( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.municipio_validator'),
                'errors' => $errors,
            ], 400);
        }

        $municipio = Municipio::create([
            'municipio' => $request->input('municipio'),
            'estado' => $request->input('estado'),
            'departamento_id' => $request->input('departamento_id'),
        ]);

        $municipio->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('general.data_create'),
            'municipio' => $municipio
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateMunicipio( $request->all(), 'update' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.municipio_validator'),
                'errors' => $errors,
            ], 400);   
        }
        
        $municipio = Municipio::find( $id );

        if( $request->input('municipio') ){
            $municipio->municipio = $request->input('municipio');
        }
        if( $request->input('estado') ){
            $municipio->estado = $request->input('estado');
        }
        if( $request->input('departamento_id') ){
            $municipio->departamento_id = $request->input('departamento_id');
        }

        $municipio->save();

        $msj = __('general.data_no_change');
        if( $municipio->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'municipio' => $municipio
        ), 200); 
    }

    /**
     * Remove the specified user from storage
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $municipio = Municipio::find( $id );
        $municipio->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('general.data_delete')
        ), 200);
    }
}
