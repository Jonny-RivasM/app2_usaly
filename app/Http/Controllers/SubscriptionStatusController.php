<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubscriptionStatus;

class SubscriptionStatusController extends Controller
{
    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $subscriptionStatus = SubscriptionStatus::all();
        return response()->json($subscriptionStatus, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $subscriptionStatus = SubscriptionStatus::find( $id );
        return response()->json($subscriptionStatus, 200); 
    }
}
