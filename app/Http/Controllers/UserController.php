<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Mail\newUserInvited;
use Illuminate\Http\Request;
use App\Traits\UserTraits;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use App\Municipio;

class UserController extends Controller
{

    use UserTraits;

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
        ]);
    }

    /**
     * List all users, it's optional return users deleted
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $soft = $request->input('soft');
        $kind = $request->input('kind');
        $numPerPages = Config::get( 'constants.records_per_page' );

        $userModel = User::with('roles');

        if( $kind ){
            $userModel->role($kind);
        }
        // $users = User::role($kind)->get();

        if( $soft ){
            $users = $userModel->onlyTrashed()->paginate(10);
        }else{
            $users = $userModel->paginate(10);
        }
        
        return response()->json($users, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $user = User::find( $id );
        return response()->json($user, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateUser( $request->all(), 'create' );
        $userLogged = User::find( auth()->user()->id );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);
        }

        $user = User::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'numid' => $request->input('numid'),
            'email' => $request->input('email'),
            'cellphone' => $request->input('cellphone'),
            'password' => bcrypt($request->input('password')),
            'municipio_id' => $request->input('municipio_id')
        ]);

        // Assign role
        if( $userLogged->hasRole('admin') || $userLogged->hasRole('moderator') ){
            if( $request->input('role_id') ){
                $role = Role::find( $request->input('role_id') );
                $user->assignRole( $role );
            }
        }else if( $userLogged->hasRole('agent') ){
            $strAuditor = Config::get( 'constants.roles.auditor' );
            $roleAuditor = Role::findByName( $strAuditor );
            $user->assignRole( $roleAuditor );
        } else {
            $strGuest = Config::get( 'constants.roles.guest' );
            $roleGuest = Role::findByName( $strGuest );
            $user->assignRole( $roleGuest );
        }

        // validate permissions
        if( !$userLogged->hasRole('admin') ){
            if ( array_key_exists( 'numid',  $request->all() ) || array_key_exists( 'email',  $request->all() ) ) { 
                return response()->json( [
                    'status' => 'error',
                    'message' => __('general.no_permissions'),
                    'errors' => $validator->errors(),
                ], 400);   
            }
        }


        $user->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('user.user_create'),
            'user' => $user
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateUser( $request->all(), 'update' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }
        
        $userLogged = User::find( auth()->user()->id );
        $user = User::where( 'id', $id )->with('roles')->first();
        $changeRole = false;
        
        if( !$user ){
            return response()->json( [
                'status' => 'error',
                'message' => __('user.not_exists')
            ], 404);   
        }
        // validate permissions
        if( !$userLogged->hasRole('admin') ){
            if ( array_key_exists( 'numid',  $request->all() ) || array_key_exists( 'email',  $request->all() ) ) { 
                return response()->json( [
                    'status' => 'error',
                    'message' => __('general.no_permissions'),
                    'errors' => $validator->errors(),
                ], 400);   
            }
        }

        // Update role
        if( $userLogged->hasRole('admin') || $userLogged->hasRole('moderator') ){
            if( $request->input('role_id') ) {
                $role = Role::find( $request->input('role_id') );
                $user->roles()->detach();
                $user->assignRole( $role );
                $changeRole = true;
            }
        }

        if( $request->input('first_name') ){
            $user->first_name = $request->input('first_name');
        }
        if( $request->input('last_name') ){
            $user->last_name = $request->input('last_name');
        }
        if( $request->input('cellphone') ){
            $user->cellphone = $request->input('cellphone');
        }
        if( $request->input('password') ){
            $user->password = $request->input('password');
        }
        if( $request->input('municipio_id') ){
            $user->municipio_id = $request->input('municipio_id');
        }

        $user->save();

        $msj = __('general.data_no_change');
        if( $user->wasChanged() || $changeRole ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'user' => $user
        ), 200); 
    }

    /**
     * Remove the specified user from storage
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find( $id );
        $user->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('user.user_delete')
        ), 200);
    }

}
