<?php

namespace App\Http\Controllers;

use App\SubscriptionPack;
use App\Company;
use App\Package;
use App\SubscriptionStatus;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Traits\SubscriptionPackTraits;
use Illuminate\Support\Facades\Config;

class SubscriptionPackController extends Controller
{
    use SubscriptionPackTraits;

    /**
     * List all users, it's optional return users deleted
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $soft = $request->input('soft');
        $numPerPages = Config::get( 'constants.records_per_page' );
        if( $soft ){
            $subscriptions = SubscriptionPack::onlyTrashed()->paginate(10);
        }else{
            $subscriptions = SubscriptionPack::paginate(10);
        }
        return response()->json($subscriptions, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $subscription = SubscriptionPack::find( $id );
        return response()->json($subscription, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateSubscriptionPack( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);
        }

        $dateIni = Carbon::parse($request->input('date_ini'));
        $dateEnd = Carbon::parse($request->input('date_end'));

        $subscriptionPack = SubscriptionPack::create([
            'date_ini' => $dateIni->toDateString(),
            'date_end' => $dateEnd->toDateString(),
            'package_id' => $request->input('package_id'),
            'company_id' => $request->input('company_id'),
            'moderator_id' => $request->input('moderator_id'),
            'subscription_status_id' => $request->input('subscription_status_id')
        ]);

        $subscriptionPack->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('subscription.user_create'),
            '$subscriptionPack' => $subscriptionPack
        ), 200); 
    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateSubscriptionPack( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }

        $subscriptionPack = SubscriptionPack::find( $id );

        if( !$subscriptionPack ){
            return response()->json( [
                'status' => 'error',
                'message' => __('subscription.not_exists')
            ], 404);   
        }

        if( $request->input('date_ini') ){
            $dateIni = Carbon::parse($request->input('date_ini'));
            $subscriptionPack->date_ini = $dateIni->toDateString();
        }
        if( $request->input('date_end') ){
            $dateEnd = Carbon::parse($request->input('date_end'));
            $subscriptionPack->date_end = $dateEnd->toDateString();
        }
        if( $request->input('package_id') ){
            $subscriptionPack->package_id = $request->input('package_id');
        }
        if( $request->input('company_id') ){
            $subscriptionPack->company_id = $request->input('company_id');
        }
        if( $request->input('moderator_id') ){
            $subscriptionPack->moderator_id = $request->input('moderator_id');
        }
        if( $request->input('subscription_status_id') ){
            $subscriptionPack->subscription_status_id = $request->input('subscription_status_id');
        }

        $subscriptionPack->save();

        $msj = __('general.data_no_change');
        if( $subscriptionPack->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'subscriptionPack' => $subscriptionPack
        ), 200); 
    }

    /**
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $subscriptionPack = SubscriptionPack::find( $id );
        $subscriptionPack->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('subscription.subscription_update')
        ), 200);
    }

}
