<?php

namespace App\Http\Controllers;

use App\Package;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Traits\PackageTraits;

class PackageController extends Controller
{
    use PackageTraits;

    function __construct(){}

    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $soft = $request->input('soft');
        $numPerPages = Config::get( 'constants.records_per_page' );
        if( $soft ){
            $packages = Package::onlyTrashed()->paginate(10);
        }else{
            $packages = Package::paginate(10);
        }
        return response()->json($packages, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $packages = Package::find( $id );
        return response()->json($packages, 200); 
    }

    /**
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $packages = Package::find( $id );
        $packages->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('package.package_delete')
        ), 200);
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validatePackage( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);
        }

        $package = Package::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'num_hilos' => $request->input('num_hilos')
        ]);

        $package->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('general.data_create'),
            'package' => $package
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validatePackage( $request->all(), 'update' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }
        
        $package = Package::where( 'id', $id )->first();
        
        if( !$package ){
            return response()->json( [
                'status' => 'error',
                'message' => __('general.package_not_exists')
            ], 404);   
        }

        if( $request->input('name') ){
            $package->name = $request->input('name');
        }
        if( $request->input('description') ){
            $package->description = $request->input('description');
        }
        if( $request->input('num_hilos') ){
            $package->num_hilos = $request->input('num_hilos');
        }
        
        $package->save();

        $msj = __('general.data_no_change');
        if( $package->wasChanged() || $changeRole ) {
            $msj = __('general.data_updated');
        }

        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'package' => $package
        ), 200); 
    }
}
