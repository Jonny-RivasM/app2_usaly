<?php

namespace App\Http\Controllers;

use App\User;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Traits\CommentTraits;

class CommentController extends Controller
{
    use CommentTraits;

    /**
     * @param Request $request
     */
    public function list(Request $request, $commentableId)
    {
        $comments = Comment::where('commentable_id', $commentableId)->get();
        return response()->json($comments, 200); 
    }

    /**
     * Method to get comment of Commentable object
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $idCommetable)
    {
        $comment = Comment::find( $idCommetable );
        return response()->json($comment, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request, $model, $idModel)
    {
        if( !$model ){
            return response()->json( [
                'status' => 'error',
                'message' => __('general.model_not_exists'),
                'errors' => $errors,
            ], 404);
        }

        if( !$idModel ) {
            return response()->json( [
                'status' => 'error',
                'message' => __('general.id_model_not_exists'),
                'errors' => $errors,
            ], 404);
        }

        $validator = $this->validateComment( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }
            return response()->json( [
                'status' => 'error',
                'message' => __('company.company_validator'),
                'errors' => $errors,
            ], 400);
        }

        $namespace = '\\App\\';
        $model = $namespace . $model; 
        $user_id = auth()->user()->id;

        $comment = $model::find($idModel)->comments()->create([
            'text' => $request->input('text'),
            'user_id' => $user_id,
            'assign_id' => $request->input('assign_id')
        ]);

        return response()->json( $comment );
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateComment( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 

            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }
            return response()->json( [
                'status' => 'error',
                'message' => __('company.company_validator'),
                'errors' => $errors,
            ], 400);
        }

        $comment = Comment::find($id);
        if( $request->input('text') ){
            $comment->text = $request->input('text');
        }
        if( $request->input('assign_id') ){
            $comment->assign_id = $request->input('assign_id');
        }
        $comment->save();

        $msj = __('general.data_no_change');
        if( $comment->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'comment' => $comment
        ), 200); 
    }

    /**
     * Remove the specified user from storage
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $comment = Comment::find( $id );
        $comment->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('comment.comment_delete')
        ), 200);
    }
}
