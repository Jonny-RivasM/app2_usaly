<?php

namespace App\Http\Controllers;

use App\Hilo;
use App\Package;
use App\Priority;
use App\Status;
use App\User;
use App\Company;
use App\SubscriptionPack;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Traits\HiloTraits;
use App\Traits\DocumentTraits;

class HiloController extends Controller
{

    use HiloTraits, DocumentTraits;

    /**
     * List all users, it's optional return users deleted
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $numPerPages = Config::get( 'constants.records_per_page' );
        $hilos = Hilo::with('moderators')
                       ->with('status')
                       ->with('priority')
                       ->with('subscription')
                       ->with('tasks')
                       ->with('comments.owner')
                       ->with('comments.assigned')
                       ->paginate( $numPerPages );
        return response()->json($hilos, 200); 
    }

    /**
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $id)
    {
        $hilo = Hilo::find( $id );
        return response()->json($hilo, 200); 
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = $this->validateHilo( $request->all(), 'create' );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 
            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);
        }

        $subscriptionPack = SubscriptionPack::find( $request->input('subscription_pack_id') );
        if( !$subscriptionPack ){
            return response()->json( [
                'status' => 'error',
                'message' => __('subscription.not_exists')
            ], 404);   
        }

        $hilo = Hilo::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'date_ini' => Carbon::parse($request->input('date_ini'))->toDateString(),
            'date_end' => Carbon::parse($request->input('date_end'))->toDateString(),
            'status_id' => $request->input('status_id'),
            'priority_id' => $request->input('priority_id'),
            'subscription_pack_id' => $request->input('subscription_pack_id')
        ]);

        $hilo->save();

        return response()->json( array(
            'status' => 'success',
            'message' => __('hilo.hilo_create'),
            'hilo' => $hilo
        ), 200); 
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validateHilo( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 
            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }

        $hilo = Hilo::where( 'id', $id )->first();
        
        if( !$hilo ){
            return response()->json( [
                'status' => 'error',
                'message' => __('hilo.not_exists')
            ], 404);   
        }

        if( $request->input('name') ){
            $hilo->name = $request->input('name');
        }
        if( $request->input('description') ){
            $hilo->description = $request->input('description');
        }
        if( $request->input('date_ini') ){
            $hilo->date_ini = $request->input('date_ini');
        }
        if( $request->input('date_end') ){
            $hilo->date_end = $request->input('date_end');
        }
        if( $request->input('status_id') ){
            $hilo->status_id = $request->input('status_id');
        }
        if( $request->input('priority_id') ){
            $hilo->priority_id = $request->input('priority_id');
        }
        if( $request->input('subscription_pack_id') ){
            $hilo->subscription_pack_id = $request->input('subscription_pack_id');
        }

        $hilo->save();

        $msj = __('general.data_no_change');
        if( $hilo->wasChanged() ) {
            $msj = __('general.data_updated');
        }
        return response()->json(array(
            'status' => 'success',
            'message' => $msj,
            'hilo' => $hilo
        ), 200); 
    }

    /**
     * 
     */
    public function addModerator(Request $request, $idHilo, $idUser) {

        $user = User::find($idUser);
        if( !$user ){
            return response()->json(array(
                'status' => 'success',
                'message' => __('user.not_exists')
            ), 200);
        }
        $hilo = Hilo::with('moderators')->where('id', $idHilo)->first();
        if( !$hilo ){
            return response()->json(array(
                'status' => 'success',
                'message' => __('hilo.not_exists')
            ), 200);
        }
        $moderatorsExistsInCompay = $hilo->moderators()->whereKey($user)->exists();
        // Verify if user exists inside a company
        if( $moderatorsExistsInCompay ) {
            return response()->json(array(
                'status' => 'success',
                'message' => __('hilo.already_exists_moderator')
            ), 200);
        }
        $hilo->moderators()->attach( $user );
        // $hilo->created_at = Carbon::now();
        // $hilo->updated_at = Carbon::now();
        $hilo->save();
        return response()->json(array(
            'status' => 'success',
            'message' => __('hilo.moderator_associated')
        ), 200); 
    }

    /**
     *
     * @param number $id
     */
    public function addDocument(Request $request, $idHilo)
    {
        $validator = $this->validateDocument( $request->all() );

        if ( array_key_exists('error', $validator) || $validator->fails() ) { 
            $errors = $validator;
            // Check if validator returns error
            if( method_exists($validator, 'errors') ){
                $errors = $validator->errors();
            }

            return response()->json( [
                'status' => 'error',
                'message' => __('general.data_validator'),
                'errors' => $errors,
            ], 400);   
        }

        $path = $this->uploadDocument( $request->file('document'), 'hilo-'.$idHilo );

        $document = Hilo::find( $idHilo )->documents()->create([
            'path' => $path
        ]);

        return response()->json(array(
            'status' => 'success',
            'message' => __('general.document_uploaded'),
            'document' => $document
        ), 200);
    }

    public function readFile(Request $request, $idFile) {
        if( $request->file('file') ){
            return response()->json( [
                'status' => 'error',
                'message' => __('general.document_not_exists')
            ], 404);
        }
    }

    /**
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $hilo = Hilo::find( $id );
        $hilo->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('hilo.hilo_delete')
        ), 200);
    }
}
