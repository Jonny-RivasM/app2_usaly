<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\DocumentTraits;
use App\Document;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class DocumentController extends Controller
{
    use DocumentTraits;

    /**
     * Method to get comment of Commentable object
     * @param Request $request
     * @param Number $id
     */
    public function find(Request $request, $idCommetable)
    {
        $comment = Comment::find( $idCommetable );
        return response()->json($comment, 200); 
    }

    /**
     * Method to get comment of Commentable object
     * @param Request $request
     * @param Number $id
     */
    public function read(Request $request, $id)
    {
        $document = Document::find( $id );
        // file not found
        if( !$document || !File::exists(storage_path($document->path)) ) {
            return response()->json(array(
                'status' => 'error',
                'message' => __('general.document_not_exists')
            ), 404);
        }

        $documentContent = File::get(storage_path($document->path));
        // for pdf, it will be 'application/pdf'
        $type       = File::mimeType(storage_path($document->path));
        $fileName   = File::name(storage_path($document->path));
  
        return Response::make($documentContent, 200, [
            'Content-Type'        => $type,
            'Content-Disposition' => 'inline; filename="'.$fileName.'"'
        ]);
    }

    /**
     * Remove the specified user from storage
     *
     * @param number $id
     */
    public function destroy(Request $request, $id)
    {
        $document = Document::find( $id );
        $document->delete();
        return response()->json(array(
            'status' => 'success',
            'message' => __('general.document_delete')
        ), 200);
    }
}
