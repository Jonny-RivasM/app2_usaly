<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HiloCheckList extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id', 
        'name',
        'description', 
        'hilo_id', 
        'completed',
    ];

    public function hilo(){
        return $this->belongsTo(Hilo::class);
    }
}
