<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 
        'text',
        'user_id', 
        'assign_id', 
        'hilo_id',
    ];

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function assigned(){
        return $this->belongsTo(User::class, 'assign_id');
    }

    public function commentable(){
        return $this->morphTo();
    }
}
