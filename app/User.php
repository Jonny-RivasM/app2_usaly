<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'numid', 
        'cellphone', 
        'municipio_id', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmail);
    }

    public function hilos(){
        return $this->belongsToMany(Hilo::class)->withTimestamps();
    }

    public function subscriptionPack(){
        return $this->hasOne(SubscriptionPack::class)->withTimestamps();
    }

    public function municipio(){
        return $this->belongsTo(Municipio::class);
    }

    public function companies(){
        return $this->belongsToMany(Company::class)->withTimestamps();
    }

    public function documents(){
      return $this->morphMany(Document::class, 'documentable');
    } 

    public function ownerComments(){
        return $this->hasOne(Comment::class, 'user_id');
    }

    public function commentsAssined(){
        return $this->belongsTo(Comment::class. 'assign_id');
    }
}
