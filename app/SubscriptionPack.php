<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPack extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_ini', 
        'date_end', 
        'package_id', 
        'company_id', 
        'moderator_id', 
        'subscription_status_id'
    ];

    public function company(){
        return $this->belongsTo(Company::class)->withTimestamps();
        // return $this->hasOne(Company::class);
    }

    public function package(){
        // return $this->hasOne(Package::class);
        return $this->belongsTo(Package::class)->withTimestamps();
    }
    
    public function status(){
        // return $this->hasOne(SubscriptionStatus::class);
        return $this->belongsTo(SubscriptionStatus::class);
    }

    public function moderator(){
        return $this->belongsTo(User::class)->withTimestamps();
    }

    public function hilos(){
        return $this->hasMany(Hilo::class);
    }
}
