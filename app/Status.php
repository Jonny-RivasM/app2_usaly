<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function hilo(){
        return $this->hasOne(Hilo::class);
        // return $this->hasMany(Hilo::class);
    }
}
