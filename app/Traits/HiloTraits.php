<?php
namespace App\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

trait HiloTraits{
    public function validateHilo($data = array(), $type = null){

        $customMessages = [
            'name.required' => __('general.name_required'),
            'description.required' => __('general.description_required'),
            'date_ini.required' => __('general.date_ini_required'),
            'date_end.required' => __('general.date_end_required'),
            'status_id.required' => __('general.status_id_required'),
            'priority_id.required' => __('general.priority_id_required'),
            'subscription_pack_id.required' => __('general.subscription_pack_id_required')
        ];

        $valids = array();
        
        if( $type == 'create' ){
            $valids = array(
                'name'    => 'required',
                'description'     => 'required',
                'date_ini' => 'required',
                'date_end'    => 'required',
                'status_id'     => 'required',
                'priority_id' => 'required',
                'subscription_pack_id' => 'required'
            );
        }

        return Validator::make( $data, $valids, $customMessages );
    }

    public function validateTaskHilo($data = array(), $type = null){

        $customMessages = [
            'name.required' => __('hilo.name_required'),
            'description.required' => __('hilo.description_required'),
            'hilo_id.required' => __('hilo.hilo_id_required')
        ];

        $valids = array();
        
        if( $type == 'create' ){
            $valids = array(
                'name'    => 'required',
                'description'     => 'required',
                'hilo_id'    => 'required'
            );
        }

        return Validator::make( $data, $valids, $customMessages );
    }
}