<?php
namespace App\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

trait SubscriptionPackTraits{
    public function validateSubscriptionPack($data = array(), $type = null){

        $customMessages = [
            'date_ini.required' => __('subscription.date_ini_required'),
            'date_end.required' => __('subscription.date_end_required'),
            'package_id.required' => __('subscription.package_id_required'),
            'company_id.required' => __('subscription.company_id_required'),
            'moderator_id.required' => __('subscription.moderator_id_required'),
            'subscription_status_id.required' => __('subscription.subscription_status_id_required')
        ];
        
        if( $type == 'create' ){
            $valids = array(
                'date_ini'    => 'required',
                'date_end'     => 'required',
                'package_id' => 'required',
                'company_id'    => 'required',
                'moderator_id'     => 'required',
                'subscription_status_id' => 'required'
            );
        }else{
            $valids = array();
        }

        return Validator::make( $data, $valids, $customMessages );
    }
}