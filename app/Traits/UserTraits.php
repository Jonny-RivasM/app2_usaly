<?php
namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

trait UserTraits{

    /** 
     * Method to valid request data
     */
    public function validateUser($data = array(), $type = null){

        if( !$type ) {
            return array(
                'error' => __('Type validate must send in request')
            );
        }

        $customMessages = [
            'email.unique' => __('general.email_unique'),
            'numid.unique' => __('general.numid_unique'),
            'numid.required' => __('general.numid_required'),
            'email.required' => __('general.email_required'),
            'municipio_id.required' => __('general.municipio_id_required'),
            'first_name.required' => __('general.first_name_required'),
            'last_name.required' => __('general.last_name_required'),
            'password.required' => __('general.password_required'),
            'c_password.required' => __('general.c_password_required'),
            'cellphone.required' => __('general.cellphone_required'),
        ];

        $valids = array();
        
        if( $type === 'create' ){
            $valids = array(
                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'unique:users|required|string|email|max:255',
                'numid'         => 'unique:users|required|max:15',
                'password'      => 'required|string|min:6|max:10', 
                'c_password'    => 'required|same:password',
                'municipio_id'  => 'required',
                'cellphone'     => 'required',
            );
        }else{
            $valids = array(
                'first_name'    => 'string|max:255',
                'last_name'     => 'string|max:255',
                'password'      => 'string|min:6|max:10'
            );
        }
        // print_r($valids);
        // exit();
        // return $data;
        return Validator::make( $data, $valids, $customMessages );
    }
    
    public function asignUserToRole($user, $role=null){
        $objRole = Role::findByName( $role, 'api');
        $user->syncRoles( $objRole );
    }


}