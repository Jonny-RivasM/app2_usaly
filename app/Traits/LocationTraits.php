<?php
namespace App\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

trait LocationTraits{
    public function validateMunicipio($data = array(), $type = null){
        if( !$type ) {
            return array(
                'error' => __('Type validate must send in request')
            );
        }

        $customMessages = [
            'municipio.unique' => __('general.municipio_required'),
            'estado.unique' => __('general.estado_required'),
            'departamento_id.required' => __('general.departamento_id_required')
        ];
        
        if( $type == 'create' ){
            $valids = array(
                'municipio'    => 'required',
                'estado'     => 'required',
                'departamento_id' => 'required'
            );
        }else{
            $valids = array();
        }

        return Validator::make( $data, $valids, $customMessages );
    }
}