<?php
namespace App\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

trait PackageTraits{
    public function validatePackage($data = array(), $type = null){

        $customMessages = [
            'name.required' => __('general.name_required'),
            'description.required' => __('general.description_required'),
            'num_hilos.required' => __('general.num_hilos_required')
        ];
        
        if( $type == 'create' ){
            $valids = array(
                'name'    => 'required',
                'description'     => 'required',
                'num_hilos' => 'required'
            );
        }else{
            $valids = array();
        }

        return Validator::make( $data, $valids, $customMessages );
    }
}