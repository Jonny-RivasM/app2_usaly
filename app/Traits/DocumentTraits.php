<?php
namespace App\Traits;

use App\Document;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

trait DocumentTraits{

    /** 
     * Method to valid request data
     */
    public function validateDocument($data = array()){

        $customMessages = [
            'document.required' => __('general.path_required')
        ];

        $valids = array(
            'document' => 'required|file|max:10240'
        );
        return Validator::make( $data, $valids, $customMessages );
    }

    /**
     * @param File $file
     * @return String Path file uploaded
     */
    public function uploadDocument( $file, $nameFolder ){
        try{
            $path = storage_path().'/app/docs/'.$nameFolder;
            if ( !File::exists($path) ) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $name_file = str_random(4).'-'.$file->getClientOriginalName();
            $file = $file->move($path, $name_file);
            return '/app/docs/'.$nameFolder.'/'.$name_file;
        }catch( Exception $e ){
            exit($e);
        }

    }

    /**
     * @param File $file
     * @return String Path file uploaded
     */
    public function changeLogoCompany( $file, $currentFile, $nameFolder ){
        try{
            $currentPathFile = storage_path().$currentFile;
            if(is_file( $currentPathFile )){
                // Remove current image
                unlink( $currentPathFile );
            }
            return $this->uploadLogoCompany( $file, $nameFolder );
        }catch( Exception $e ){
            exit($e);
        }

    }
}