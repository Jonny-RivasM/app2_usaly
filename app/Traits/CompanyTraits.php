<?php
namespace App\Traits;

use App\Company;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

trait CompanyTraits{

    /** 
     * Method to valid request data
     */
    public function validateCompany($data = array(), $type = null){

        $customMessages = [
            'name.required' => __('company.name_required'),
            'num_id.required' => __('company.num_id_required'),
            'address.required' => __('company.address_required'),
            'phone.required' => __('company.phone_required'),
            'logo.file' => __('company.logo_file'),
            'municipio_id.required' => __('company.municipio_id_required')
        ];

        $valids = array();
        
        if( $type === 'create' ){
            $valids = array(
                'name' => 'required',
                'num_id' => 'required|unique:companies',
                'address' => 'required|string|max:255',
                'phone' => 'required|string',
                // 'logo' => 'string',
                'logo' => 'file|max:10240',
                'municipio_id' => 'required',
            );
        }

        return Validator::make( $data, $valids, $customMessages );
    }

    /**
     * @param File $file
     * @param Number $idCompany
     * @return String Path file uploaded
     */
    public function uploadLogoCompany( $file, $idCompany ){
        try{
            $path = public_path().'/companies/'.$idCompany;
            if ( !File::exists($path) ) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            $name_file = str_random(4).'-'.$file->getClientOriginalName();
            $file = $file->move($path, $name_file);
            return '/companies/'.$idCompany.'/'.$name_file;
            // return $path."/".$name_file;
        }catch( Exception $e ){
            exit($e);
        }

    }

    /**
     * @param File $file
     * @param Number $idCompany
     * @return String Path file uploaded
     */
    public function changeLogoCompany( $file, $currentLogo, $idCompany ){
        try{
            $currentPathLogo = public_path().$currentLogo;
            // if ( !File::exists($currentPathLogo) ) {
            //     Storage::delete($currentPathLogo);
            // }
            if(is_file( $currentPathLogo )){
                // Remove current image
                unlink( $currentPathLogo );
            }

            return $this->uploadLogoCompany( $file, $idCompany );

        }catch( Exception $e ){
            exit($e);
        }

    }
}