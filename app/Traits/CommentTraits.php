<?php
namespace App\Traits;

use App\Company;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

trait CommentTraits{
    /** 
     * Method to valid request data
     */
    public function validateComment($data = array(), $type = null){
        $customMessages = [
            'text.required' => __('comment.text_required'),
            'user_id.required' => __('comment.user_id_required'),
            'assign_id.required' => __('comment.assign_id_required'),
            'commentable_id.required' => __('comment.commentable_id_required'),
            'commentable_type.file' => __('comment.commentable_type_required'),
        ];

        $valids = array();
        
        if( $type === 'create' ){
            $valids = array(
                'text' => 'required',
                'user_id' => 'required',
                'assign_id' => 'required',
                'commentable_id' => 'required',
                'commentable_type' => 'required',
            );
        }

        return Validator::make( $data, $valids, $customMessages );
    }
}