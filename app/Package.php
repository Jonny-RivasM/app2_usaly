<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    protected $fillable = [
        'name',
        'description',
        'num_hilos'
    ];

    use SoftDeletes;

    public function subscription(){
        return $this->hasOne(SubscriptionPack::class)->withTimestamps();
    }
}
