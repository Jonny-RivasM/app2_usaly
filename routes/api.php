<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('/login', 'UserController@login')->name('users.login');
Route::group([
    'middleware' => ['auth:api']
], function () {
    Route::get('/priority/list', 'PriorityController@list')->name('priority.list');
    Route::get('/priority/{id}', 'PriorityController@find')->name('priority.find');

    Route::get('/status/list', 'StatusController@list')->name('status.list');
    Route::get('/status/{id}', 'StatusController@find')->name('status.find');

    Route::get('/subscription-status/list', 'SubscriptionStatusController@list')->name('subscription_status.list');
    Route::get('/subscription-status/{id}', 'SubscriptionStatusController@find')->name('subscription_status.find');

    Route::get('/document/{id}', 'DocumentController@read')->name('document.read');
    
    // Users routes
    Route::group([
        'middleware' => ['role:admin|moderator|agent']
    ], function() {
            Route::put('/user/{id}', 'UserController@update')->name('user.update');
            Route::post('/user', 'UserController@create')->name('user.create');

            // Route::post('/document/{model}/{idModel}', 'DocumentController@create')->name('document.create');
            // Route::delete('/document/{id}', 'DocumentController@destroy')->name('document.remove');

            // ----

            Route::get('/comment/{idCommetable}/list', 'CommentController@list')->name('comment.list');
            Route::get('/comment/{idCommetable}/list/{idComment}', 'CommentController@list')->name('comment.list');
            Route::post('/comment/{model}/{idModel}', 'CommentController@create')->name('comment.create');
            Route::put('/comment/{id}', 'CommentController@update')->name('comment.update');
            Route::delete('/comment/{id}', 'CommentController@destroy')->name('comment.remove');
            Route::get('/comment/{id}', 'CommentController@find')->name('comment.find');

            Route::get('/hilo/list', 'HiloController@list')->name('hilo.list');
            Route::get('/hilo/{id}', 'HiloController@find')->name('hilo.find');
            Route::put('/hilo/{id}', 'HiloController@update')->name('hilo.update');
            Route::post('/hilo', 'HiloController@create')->name('hilo.create');
            Route::delete('/hilo/{id}', 'HiloController@destroy')->name('hilo.remove');

            Route::post('/hilo/{idHilo}/add-document', 'HiloController@addDocument')->name('hilo.add_document');
            
            Route::get('/task/list', 'HiloCheckListController@list')->name('task.list');
            Route::get('/task/{id}', 'HiloCheckListController@find')->name('task.find');
            Route::put('/task/{id}', 'HiloCheckListController@update')->name('task.update');
            Route::post('/task', 'HiloCheckListController@create')->name('task.create');
            Route::delete('/task/{id}', 'HiloCheckListController@destroy')->name('task.remove');
        }
    );
    Route::group([
        'middleware' => ['role:admin|moderator']
    ], function() {

            Route::post('/hilo/{idHilo}/add-moderator/{idUser}', 'HiloController@addModerator')->name('hilo.add_user');

            Route::get('/package/list', 'PackageController@list')->name('package.list');
            Route::get('/package/{id}', 'PackageController@find')->name('package.find');
            Route::put('/package/{id}', 'PackageController@update')->name('package.update');
            Route::post('/package', 'PackageController@create')->name('package.create');
            Route::delete('/package/{id}', 'PackageController@destroy')->name('package.remove');
            
            Route::get('/subscription/list', 'SubscriptionPackController@list')->name('subscription.list');
            Route::get('/subscription/{id}', 'SubscriptionPackController@find')->name('subscription.find');
            Route::put('/subscription/{id}', 'SubscriptionPackController@update')->name('subscription.update');
            Route::post('/subscription', 'SubscriptionPackController@create')->name('subscription.create');
            Route::delete('/subscription/{id}', 'SubscriptionPackController@destroy')->name('subscription.remove');

            Route::get('/user/list', 'UserController@list')->name('user.list');
            Route::get('/user/{id}', 'UserController@find')->name('user.find');
            Route::delete('/user/{id}', 'UserController@destroy')->name('user.remove');

            Route::put('/municipio/{id}', 'MunicipioController@update')->name('municipio.update');
            Route::post('/municipio', 'MunicipioController@create')->name('municipio.create');
            Route::get('/municipio/list', 'MunicipioController@list')->name('municipio.list');
            Route::get('/municipio/{id}', 'MunicipioController@find')->name('municipio.find');
            Route::delete('/municipio/{id}', 'MunicipioController@destroy')->name('municipio.remove');

            Route::get('/company/list', 'CompanyController@list')->name('company.list');
            Route::post('/company', 'CompanyController@create')->name('company.create');
            Route::post('/company/{id}', 'CompanyController@update')->name('company.update'); // Put not workgin with form-data requests (files)
            Route::get('/company/{id}', 'CompanyController@find')->name('company.find');
            Route::delete('/company/{id}', 'CompanyController@destroy')->name('company.remove');
            Route::post('/company/{idCompany}/add-user/{idUser}', 'CompanyController@addUser')->name('company.add_user');
        }
    );
});