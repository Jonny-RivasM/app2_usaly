<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Agent
     * Moderator
     * Auditor
     * Admin
     */



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        app()['cache']->forget('spatie.permission.cache');

        Permission::create([ 'guard_name' => 'api', 'name' => 'permissions.users.read' ]);
        Permission::create([ 'guard_name' => 'api', 'name' => 'permissions.company.read' ]);
        Permission::create([ 'guard_name' => 'api', 'name'=>'permissions.users.update' ]);
        Permission::create([ 'guard_name' => 'api', 'name'=>'permissions.users.create' ]);
        Permission::create([ 'guard_name' => 'api', 'name'=>'permissions.users.delete' ]);
        Permission::create([ 'guard_name' => 'api', 'name'=>'permissions.location.create' ]);

        

        $strAgent = Config::get( 'constants.roles.agent' );
        $agent = Role::create( [ 'name' => $strAgent ] );
        $agent->givePermissionTo([]);

        $strModerator = Config::get( 'constants.roles.moderator' );
        $moderator = Role::create( [ 'name' => $strModerator ] );
        $moderator->givePermissionTo([]);

        $strAuditor = Config::get( 'constants.roles.auditor' );
        $auditor = Role::create( [ 'name' => $strAuditor ] );
        $auditor->givePermissionTo([]);

        $strAdmin = Config::get( 'constants.roles.admin' );
        $admin = Role::create( [ 'name' => $strAdmin ] );
        $admin->givePermissionTo([]);
    }
}
