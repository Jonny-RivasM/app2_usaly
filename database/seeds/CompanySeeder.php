<?php

use Illuminate\Database\Seeder;
use App\Company;
use App\User;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Company::create([
            'id' => 2,
            'name' => 'Motivo',
            'num_id' => '33456742',
            'address' => 'Calle nose donde queda # 1 - 2',
            'phone' => 33473648,
            'municipio_id' => 150
            // 'location' => 'Cali'
        ]);

        $usaly = Company::create([
            'id' => 1,
            'name' => 'Usaly',
            'num_id' => '901356825-6',
            'address' => 'Calle 10 # 29B-21',
            'phone' => 3008915853,
            'municipio_id' => 150
            // 'location' => 'Cali'
        ]);

        // $rrivas = User::where('email', 'rrivas@usaly.co');

        // $usaly->users()->detach($rrivas);
    }
}
