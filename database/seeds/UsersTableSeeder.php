<?php

use App\User;
use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => 'jonrimo@gmail.com',
            'first_name' => 'Jonny',
            'last_name' => 'Rivas Montaño',
            'password' => bcrypt('123456789'),
            'email_verified_at' => '2020-03-16 15:16:29',
            'municipio_id' => 150
        ]);
        
        
        $rrivas = User::create([
            'email' => 'rrivas@usaly.co',
            'first_name' => 'Richard',
            'last_name' => 'Rivas Montaño',
            'password' => bcrypt('123456789'),
            'email_verified_at' => '2020-03-16 15:16:29',
            'municipio_id' => 150
        ]);
        $strAdmin = Config::get( 'constants.roles.admin' );
        $roleAdmin = Role::findByName( $strAdmin, 'web');
        $rrivas->assignRole( $roleAdmin );
            
        $usalyCompany = Company::find(1);
        $usalyCompany->users()->attach( $rrivas );


        $user3 = User::create([
            'email' => 'i.ricard26@gmail.com',
            'first_name' => 'Carlos Andrade',
            'last_name' => 'Rodriguez Mosquera',
            'password' => bcrypt('123456789'),
            'email_verified_at' => '2020-03-16 15:16:29',
            'municipio_id' => 150
        ]);
    }
}
