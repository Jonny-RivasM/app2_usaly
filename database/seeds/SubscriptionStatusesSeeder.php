<?php

use Illuminate\Database\Seeder;
use App\SubscriptionStatus;

class SubscriptionStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SubscriptionStatus::create([
            'name' => 'Activa',
            'description' => 'Mebresía activa'
        ]);

        SubscriptionStatus::create([
            'name' => 'Desactivada',
            'description' => 'Mebresía desactivada'
        ]);

        SubscriptionStatus::create([
            'name' => 'Vencida',
            'description' => 'Mebresía caducada'
        ]);

        SubscriptionStatus::create([
            'name' => 'Bloqueada',
            'description' => 'Mebresía bloqueada'
        ]);
    }
}
