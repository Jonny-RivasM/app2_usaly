<?php

use Illuminate\Database\Seeder;

class MunicipiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $path = database_path('vendor/locations/municipios.sql');
        DB::unprepared(file_get_contents($path));
        $this->command->info('Seeder municipios file successfully');
    }
}
