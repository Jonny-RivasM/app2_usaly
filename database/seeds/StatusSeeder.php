<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        
        $status = Status::create([
            'name' => 'in_process',
            'label' => 'En proceso'
        ]);
        $status2 = Status::create([
            'name' => 'completed',
            'label' => 'Completado'
        ]);
        $status3 = Status::create([
            'name' => 'actived',
            'label' => 'Actived'
        ]);
    }
}
