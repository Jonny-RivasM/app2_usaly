<?php

use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $path = database_path('vendor/locations/departamentos.sql');
        DB::unprepared(file_get_contents($path));
        $this->command->info('Seeder departamentos file successfully');
    }
}
