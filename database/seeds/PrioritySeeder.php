<?php

use App\Priority;
use Illuminate\Database\Seeder;

class PrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Priority::create([
            'name' => 'high',
            'label' => 'Alta'
        ]);
        $user = Priority::create([
            'name' => 'low',
            'label' => 'Baja'
        ]);
        $user = Priority::create([
            'name' => 'normal',
            'label' => 'Normal'
        ]);
    }
}
