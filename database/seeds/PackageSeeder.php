<?php

use Illuminate\Database\Seeder;
use App\Package;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Package::create([
            'name' => 'Middle',
            'description' => 'Paquete diseño y soporte',
            'num_hilos' => 100
        ]);
    }
}
