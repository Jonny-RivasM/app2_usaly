<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiloCheckListsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hilo_check_lists';

    /**
     * Run the migrations.
     * @table hilo_check_lists
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('completed')->nullable()->default('0');
            $table->unsignedInteger('hilo_id');
            $table->timestamps();

            $table->index(["hilo_id"], 'fk_hilo_check_lists_hilos1_idx');


            $table->foreign('hilo_id', 'fk_hilo_check_lists_hilos1_idx')
                ->references('id')->on('hilos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}