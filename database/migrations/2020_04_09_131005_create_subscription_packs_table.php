<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPacksTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'subscription_packs';

    /**
     * Run the migrations.
     * @table subscription_packs
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->dateTime('date_ini')->nullable();
            $table->dateTime('date_end')->nullable();
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('moderator_id');
            $table->unsignedInteger('subscription_status_id');
            $table->timestamps();

            $table->index(["company_id"], 'fk_subscription_packs_companies1_idx');

            $table->index(["moderator_id"], 'fk_subscription_packs_users1_idx');

            $table->index(["subscription_status_id"], 'fk_subscription_packs_subscription_statuses1_idx');

            $table->index(["package_id"], 'fk_subscription_packs_packages1_idx');


            $table->foreign('package_id', 'fk_subscription_packs_packages1_idx')
                ->references('id')->on('packages')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('company_id', 'fk_subscription_packs_companies1_idx')
                ->references('id')->on('companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('moderator_id', 'fk_subscription_packs_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('subscription_status_id', 'fk_subscription_packs_subscription_statuses1_idx')
                ->references('id')->on('subscription_statuses')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}