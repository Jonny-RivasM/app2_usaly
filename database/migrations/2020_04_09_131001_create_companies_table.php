<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'companies';

    /**
     * Run the migrations.
     * @table companies
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('num_id', 45)->nullable();
            $table->string('address', 150)->nullable();
            $table->bigInteger('phone')->nullable();
            $table->text('logo')->nullable();
            $table->unsignedInteger('municipio_id');
            $table->timestamps();

            $table->index(["municipio_id"], 'fk_companies_municipios1_idx');

            $table->unique(["num_id"], 'num_id_UNIQUE');


            $table->foreign('municipio_id', 'fk_companies_municipios1_idx')
                ->references('id_municipio')->on('municipios')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}