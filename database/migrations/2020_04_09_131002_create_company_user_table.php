<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'company_user';

    /**
     * Run the migrations.
     * @table company_user
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->tinyInteger('is_agent')->nullable()->default('0');
            $table->tinyInteger('receive_notify')->nullable()->default('0');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->index(["user_id"], 'fk_company_users_users1_idx');

            $table->index(["company_id"], 'fk_company_users_companies1_idx');


            $table->foreign('company_id', 'fk_company_users_companies1_idx')
                ->references('id')->on('companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'fk_company_users_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}