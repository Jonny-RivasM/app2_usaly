<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiloUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hilo_user';

    /**
     * Run the migrations.
     * @table hilo_user
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('hilo_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->index(["hilo_id"], 'fk_hilos_users_hilos1_idx');

            $table->index(["user_id"], 'fk_hilos_users_users1_idx');


            $table->foreign('hilo_id', 'fk_hilos_users_hilos1_idx')
                ->references('id')->on('hilos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'fk_hilos_users_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}