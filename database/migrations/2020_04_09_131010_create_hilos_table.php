<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHilosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'hilos';

    /**
     * Run the migrations.
     * @table hilos
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->text('description')->nullable();
            $table->date('date_ini')->nullable();
            $table->date('date_end')->nullable();
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('priority_id');
            $table->unsignedInteger('subscription_pack_id');
            $table->timestamps();

            $table->index(["priority_id"], 'fk_hilos_priorities1_idx');

            $table->index(["status_id"], 'fk_hilos_statuses1_idx');

            $table->index(["subscription_pack_id"], 'fk_hilos_subscription_packs1_idx');


            $table->foreign('status_id', 'fk_hilos_statuses1_idx')
                ->references('id')->on('statuses')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('priority_id', 'fk_hilos_priorities1_idx')
                ->references('id')->on('priorities')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('subscription_pack_id', 'fk_hilos_subscription_packs1_idx')
                ->references('id')->on('subscription_packs')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}