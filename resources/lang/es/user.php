<?php

return [
    'user_delete' => 'Usuario eliminado correctamente.',
    'user_create' => 'El Usuario ha sido creado correctamente.',
    'not_exists' => "El usuario no existe.",
    'already_exists_company' => "El usuario ya está relacionado en la compañía.",
];
