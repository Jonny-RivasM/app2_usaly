<?php

return [
    'company_id_required'     => 'El ID de la compañía no puede ser vacío.',
    'user_id_required'     => 'El usuario no puede estar vacío.',
    'company_validator'     => 'Hay errores en los datos enviados.',
    'company_create' => 'Company was created successfully.',
    'name_required'     => 'El nombre no puede estar vacío.',
    'num_id_required'     => 'El documento de identificación no puede estar vacío.',
    'address_required'     => 'La dirección no puede estar vacío.',
    'phone_required'     => 'El número de contacto no puede estar vacío.',
    'logo_file'     => 'El logo debe ser una archivo.',
    'municipio_id_required'     => 'El municipio no puede estar vacío.',
    'user_associated'     => "Usuario asociado correctamente.",
    'not_exists' => "La compaía no existe.",
];
