<?php

return [
    'name_required' => "El nombre no puede estar vacío.",
    'description_required' => "La descripción no puede estar vacía.",
    'date_ini_required' => "La fecha de inicio no puede estar vacía.",
    'date_end_required' => "La fecha final no puede estar vacía.",
    'status_id_required' => "El estado no puede estar vacío.",
    'priority_id_required' =>"La prioridad no puede estar vacía.",
    'subscription_pack_id_required' => "La subscripción no puede estar vacía.",
    'hilo_create' => "El hilo ha sido creado correctamente.",
    'not_exists' => "El hilo no existe.",
    'task_not_exists' => "La tarea no existe.",
    'hilo_delete' => "El hilo ha sido eliminado correctamente.",
    'task_delete' => "La tarea ha sido eliminado correctamente.",
    'already_exists_moderator' => "El moderador ya está relacionado en el hilo.",
    'moderator_associated'     => "Moderador asociado correctamente.",
    'hilo_id_required'     => "El hilo no puede estar vacío.",
    'task_create'     => "La tarea ha sido creado correctamente.",
    'document_required' => "El documento no puede estar vacío.",
];
