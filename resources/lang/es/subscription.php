<?php

return [
    'date_ini_required' => "La fecha inicial no puede estar vacía.",
    'date_end_required' => "La fecha final no puede estar vacía.",
    'package_id_required' => "El paquete no puede estar vacío.",
    'company_id_required' => "La compañía no puede estar vacío.",
    'moderator_id_required' => "El moderador no puede estar vacío.",
    'subscription_status_id_required' => "El estado de la subscripción no puede estar vacía.",
    'subscription_create' => "La subscripción ha sido creada correctamente.",
    'subscription_update' => "La subscripción ha sido actualizada correctamente.",
    'not_exists' => "La subscripción no existe."
];
