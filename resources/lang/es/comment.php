<?php

return [
    'text_required' => "El texto no puede estar vacío",
    'user_id_required' => "El ID del usuario no puede estar vacío",
    'assign_id_required' => "El usuario asignado no puede estar vacío",
    'commentable_id_required' => "El ID del Commentable no puede estar vacío",
    'commentable_type_required' => "El tipo Commentable no puede estar vacío",
    'comment_delete' =>"Comentario eliminado correctamente.",
];