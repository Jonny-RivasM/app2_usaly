<?php

return [
    'user_delete' => 'User was deleted successfully.',
    'user_create' => 'User was created successfully.',
    'not_exists' => "User doesn't exists.",
    'already_exists_company' => "User already exists in company.",
];
