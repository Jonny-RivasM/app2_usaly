<?php

return [
    'name_required' => "Name can't be empty.",
    'description_required' => "Description can't be empty.",
    'date_ini_required' => "Date start can't be empty.",
    'date_end_required' => "Date end can't be empty.",
    'status_id_required' => "Status can't be empty.",
    'priority_id_required' =>"Priority can't be empty.",
    'subscription_pack_id_required' => "Subscription can't be empty.",
    'hilo_create' => "Hilo was created successfully.",
    'not_exists' => "Hilo doesn't exists.",
    'task_not_exists' => "Task doesn't exists.",
    'hilo_delete' => "Hilo was deleted successfully.",
    'task_delete' => "Task was deleted successfully.",
    'already_exists_moderator' => "Moderator already exists in hilo.",
    'moderator_associated'     => "Moderator was associated successfully.",
    'hilo_id_required'     => "Hilo can't be empty.",
    'task_create'     => "Task was created successfully.",
    'document_required' => "Document can't be empty.",
];
