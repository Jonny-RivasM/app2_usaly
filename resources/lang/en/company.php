<?php

return [
    'company_id_required'     => "ID Company can't be empty.",
    'user_id_required'     => "ID user can't be empty.",
    'company_validator'     => 'Data sent has errors.',
    'company_create' => 'Company was created successfully.',
    'name_required'     => "Name company can't be empty.",
    'num_id_required'     => "Document ID can't be empty.",
    'address_required'     => "Address can't be empty.",
    'phone_required'     => "Contact number can't be empty.",
    'logo_string'     => "Logo must be a file.",
    'municipio_id_required'     => "Municipio can't be empty.",
    'user_associated'     => "User was associated successfully.",
    'not_exists' => "User doesn't exists.",

];
