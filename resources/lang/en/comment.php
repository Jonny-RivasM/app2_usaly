<?php

return [
    'text_required' => "Text can't be empty",
    'user_id_required' => "User ID can't be empty",
    'assign_id_required' => "Assigned ID can't be empty",
    'commentable_id_required' => "Commentable ID can't be empty",
    'commentable_type_required' => "Commentable type can't be empty",
    'comment_delete' =>"Comment was deleted successfully.",
];
