<?php

return [
    'date_ini_required' => "Start date can't empty.",
    'date_end_required' => "End date can't empty.",
    'package_id_required' => "Package can't empty.",
    'company_id_required' => "Company can't empty.",
    'moderator_id_required' => "Moderator can't empty.",
    'subscription_status_id_required' => "Subscription can't empty.",
    'subscription_create' => "Subscription was created successfully.",
    'subscription_update' => "Subscription was updated successfully.",
    'not_exists' => "Subscription doesn't exists."
];
