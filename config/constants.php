<?php
return [
    'records_per_page' => 10,
    'roles' => [
        'agent'     => 'agent',
        'moderator' => 'moderator',
        'auditor'   => 'auditor',
        'admin'     => 'admin',
    ]
];